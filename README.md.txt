============  To use git ============

- To create branch: 

	git branch YourBranchName

- To push git branch:

	git push origin YourBranchName

- To remove branch:

	git branch -d YourBranchName

- To check branch:

	git branch

- To switch branch:

	git switch YourBranchName

- To add repository go to you folder then type cmd in the top of file direction (it will load cmd)
	
	Not known yet

- To prepare to push your file

	git add . 

	git commit -m "Your message to mark your commit"

	git push #push into git

- To check status use 

	git status
 
